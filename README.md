# Ranking Generator

Ranking Generator (ranking-generator) is a part of Meta-Alert Prioritisation (MAP) module. It is a REST service providing endpoints allowing to apply rule models (MCDA models) to prioritisation (ranking). More precisely, the following REST endpoints are provided:

Name | Method | Description
---|---|---
`/meta-data` `/metadata`| `GET`| provides information about the meta-data (attributes) used in prioritisation; the output is in JSON format 
`/rules-text` | `GET` | provides (in plain text form) the rule model used for prioritisation
`/rules-ruleml` | `GET` | provides (in RuleML format) the rule model used for prioritisation 
`/rank` | `POST` | provides prioritisation (ranking) of examples passed as body of request in JSON format; the output is JSON consisting of pairs: id of example, assigned priority
`/rank-ext` | `POST` | provides prioritisation (ranking) of examples passed as body of request in JSON format; the output is JSON consisting of whole description of examples and assigned priorities

Request parameters passed to all endpoints are the same: 

- `user_id` - id of user (e.g., `protective`), when `model_id` is not passed, the active model of the user identified by `user_id` is used (provided that it is set for the user)
- `model_id` - id of the model used for prioritisation

At least one of the above defined request parameters should be passed when calling ranking-generator endpoints.

# How to install?
## Cloning repository
git clone git@gitlab.com:protective-h2020-eu/meta-alert-prioritisation/ranking-generator.git

## Building

### Managing docker images

To create and push new docker image to the repository, **gradle.properties** file needs to be filled. 
It contains registry credentials such as login, password and repository url. There are **no changes in build.gradle** needed.
After that, proper gradle task can be run (in project dir). 

**Warning:** tag of the docker image will be generated based on *annotated* git tags. 
More info: [semver-git](https://github.com/cinnober/semver-git)
### Gradle tasks
To create only Dockerfile in /build/docker, run:
```
gradlew -q createDockerfile
```

To create Dockerfile and local docker image, run:
```
gradlew -q buildImage
```

To create and push local docker image, run:
```
gradlew -q pushImage
```
**Warning:** It may take some time and create some warnings.


All above tasks can be found in *docker* task group.

# Running

In all of examples provided in this document it is assumed that ranking-generator may be accessed on a machine called `host` on port `8008`.

The following query command may be used to see meta-data used in a given model identified by `model_id = 3` for `user_id = jurek`:

```
curl -X GET -i 'http://host:8008/meta-data?user_id=jurek&model_id=3'
```

On the other hand, to see the rules used in model identified by `model_id = 3`, one may use query command:

```
curl -X GET -i 'http://host:8008/rules-text?user_id=jurek&model_id=3'
```

The result of the query above may look like:

```
1: (SourceAssetCriticality >= critical) => (Priority <= 1)
2: (SourceAssetCriticality >= high) => (Priority <= 2)
3: (SourceAssetCriticality >= med) & (TargetAssetCriticality >= critical) & (SeverityForAttackCategory >= high) => (Priority <= 2)
4: (TargetAssetCriticality >= critical) & (TimeToDueDate <= 18.59) & (AttackSourceReputationScore >= 0.75) & (MaxCVE >= 8.2) => (Priority <= 2)
5: (SourceAssetCriticality >= med) => (Priority <= 3)
6: (SourceAssetCriticality >= low) & (TargetAssetCriticality >= critical) & (TimeToDueDate <= 25.45) => (Priority <= 3)
7: (SourceAssetCriticality >= low) => (Priority <= 4)
8: (SourceAssetCriticality <= NA) & (SeverityForAttackCategory <= high) => (Priority >= 5)
9: (SourceAssetCriticality <= NA) => (Priority >= 4)
10: (SourceAssetCriticality <= low) & (TimeToDueDate >= 63.7) & (TimeFromDetectTime >= 36.02) => (Priority >= 4)
11: (SourceAssetCriticality <= low) => (Priority >= 3)
12: (SourceAssetCriticality <= med) & (TargetAssetCriticality <= high) => (Priority >= 3)
13: (SourceAssetCriticality <= med) & (TimeToDueDate >= 39.56) & (SeverityForAttackCategory <= med) => (Priority >= 3)
14: (SourceAssetCriticality <= med) => (Priority >= 2)
15: (SourceAssetCriticality <= high) & (TargetAssetCriticality <= low) => (Priority >= 2)
```

The following query command may be used to get the same rules in RuleML format:

```
curl -X GET -i 'http://host:8008/rules-ruleml?user_id=jurek&model_id=3'
```



Then, one may check prioritisation of the following meta-alerts with model identified by `model_id = 3`.

```json
[{
  "TargetAssetCriticality": "critical",
  "SourceAssetCriticality": "low",
  "TimeToDueDate": -100896.193,
  "TimeFromDetectTime": 187296.193,
  "MAQuality": 0.7,
  "AttackSourceReputationScore": 0.5,
  "SeverityForAttackCategory": "critical",
  "MaxCVE": 0,
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514f"
  },{
  "TargetAssetCriticality": "critical",
  "SourceAssetCriticality": "NA",
  "TimeToDueDate": -10641696.193,
  "TimeFromDetectTime": 10728096.193,
  "MAQuality": 0.7,
  "AttackSourceReputationScore": 0.5,
  "SeverityForAttackCategory": "low",
  "MaxCVE": 1,
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514e"
}]

```

To prioritise these meta-alerts, the following query command may be used:

```
curl -X POST -H "Content-Type: application/json" -i 'http://host:8008/rank?user_id=jurek&model_id=3' 
--data '[{
  "TargetAssetCriticality": "critical",
  "SourceAssetCriticality": "low",
  "TimeToDueDate": -100896.193,
  "TimeFromDetectTime": 187296.193,
  "MAQuality": 0.7,
  "AttackSourceReputationScore": 0.5,
  "SeverityForAttackCategory": "critical",
  "MaxCVE": 0,
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514f"
  },{
  "TargetAssetCriticality": "critical",
  "SourceAssetCriticality": "NA",
  "TimeToDueDate": -10641696.193,
  "TimeFromDetectTime": 10728096.193,
  "MAQuality": 0.7,
  "AttackSourceReputationScore": 0.5,
  "SeverityForAttackCategory": "low",
  "MaxCVE": 1,
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514e"
}]'
```

The resulting prioritisation (ranking) may be the following:

```json
[{
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514f",
  "Priority": "3"
}, {
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514e",
  "Priority": "5"
}]
```

There is also a possibility to obtain priorities (ranks) together with description of prioritised meta-alerts. The following query command may be used to this end:

```
curl -X POST -H "Content-Type: application/json" -i 'http://host:8008/rank-ext?user_id=jurek&model_id=3' 
--data '[{
  "TargetAssetCriticality": "critical",
  "SourceAssetCriticality": "low",
  "TimeToDueDate": -100896.193,
  "TimeFromDetectTime": 187296.193,
  "MAQuality": 0.7,
  "AttackSourceReputationScore": 0.5,
  "SeverityForAttackCategory": "critical",
  "MaxCVE": 0,
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514f"
  },{
  "TargetAssetCriticality": "critical",
  "SourceAssetCriticality": "NA",
  "TimeToDueDate": -10641696.193,
  "TimeFromDetectTime": 10728096.193,
  "MAQuality": 0.7,
  "AttackSourceReputationScore": 0.5,
  "SeverityForAttackCategory": "low",
  "MaxCVE": 1,
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514e"
}]'
```

The resulting prioritisation (ranking) with description of meta-alerts may look like:

```json
[{
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514f",
  "SourceAssetCriticality": "low",
  "TargetAssetCriticality": "critical",
  "TimeToDueDate": "-100896.193",
  "TimeFromDetectTime": "187296.193",
  "SeverityForAttackCategory": "critical",
  "MAQuality": "0.7",
  "AttackSourceReputationScore": "0.5",
  "MaxCVE": "0.0",
  "Priority": "3"
}, {
  "ID": "b70eac70-b4ac-11e7-9460-002564d9514e",
  "SourceAssetCriticality": "NA",
  "TargetAssetCriticality": "critical",
  "TimeToDueDate": "-1.0641696193E7",
  "TimeFromDetectTime": "1.0728096193E7",
  "SeverityForAttackCategory": "low",
  "MAQuality": "0.7",
  "AttackSourceReputationScore": "0.5",
  "MaxCVE": "1.0",
  "Priority": "5"
}]
```