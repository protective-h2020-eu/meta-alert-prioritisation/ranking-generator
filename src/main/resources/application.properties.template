# Gson enabled
spring.http.converters.preferred-json-mapper.gson=true

# Spring Boot Actuator configuration
management.endpoints.web.base-path=/
management.endpoint.info.enabled=true
info.app.name=Ranking Generator
info.app.description=Generates ranking (prioritisation) for meta-alerts
info.app.version=1.8.0
management.endpoint.health.enabled=true

# Application configuration
# command line option for JVM is --server.port=8008
server.port = 8008

# Postgres configuration
# Each expression \${property_name} is replaced with the value of the respective property read from selected *.template-properties file in the same directory.
# Selection of particular *.template-properties file depends on the current Git working branch - file x.template-properties is selected if current Git working branch is x.
spring.datasource.url=jdbc:postgresql://${db_host}:${db_port}/${db_name}
spring.datasource.username=${db_user_name}
spring.datasource.password=${db_user_password}
spring.datasource.driver-class-name=org.postgresql.Driver

# Added to avoid suppress an annoying exception that occurs when JPA (Hibernate) tries to verify PostgreSQL CLOB feature.
spring.jpa.database-platform=org.hibernate.dialect.PostgreSQL9Dialect
spring.jpa.properties.hibernate.temp.use_jdbc_metadata_defaults = false