/**
 * Copyright (C) Jerzy Błaszczyński, Marcin Szeląg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.h2020.protective.meta_alert_prioritisation.ranking_generator;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.rulelearn.classification.ClassificationResultSet;
import org.rulelearn.classification.SimpleClassificationResult;
import org.rulelearn.classification.SimpleClassificationResultSet;
import org.rulelearn.classification.SimpleRuleClassifier;
import org.rulelearn.data.Attribute;
import org.rulelearn.data.AttributeType;
import org.rulelearn.data.EvaluationAttribute;
import org.rulelearn.data.InformationTable;
import org.rulelearn.data.SimpleDecision;
import org.rulelearn.data.json.AttributeParser;
import org.rulelearn.data.json.ObjectParser;
import org.rulelearn.rules.RuleSet;
import org.rulelearn.rules.ruleml.RuleParser;
import org.rulelearn.types.ElementList;
import org.rulelearn.types.EnumerationField;
import org.rulelearn.types.EnumerationFieldFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import eu.h2020.protective.meta_alert_prioritisation.ranking_generator.model.Models;
import eu.h2020.protective.meta_alert_prioritisation.ranking_generator.model.ModelsRepository;
import eu.h2020.protective.meta_alert_prioritisation.ranking_generator.model.Result;
import eu.h2020.protective.meta_alert_prioritisation.ranking_generator.model.Users;
import eu.h2020.protective.meta_alert_prioritisation.ranking_generator.model.UsersRepository;
import eu.h2020.protective.meta_alert_prioritisation.ranking_generator.model.json.JSONString;
import it.unimi.dsi.fastutil.ints.IntIterator;

/**
 * Spring Boot REST controller wrapping ruleLearn ranking (ordinal classification) methods.
 *
 * @author Jerzy Błaszczyński (<a href="mailto:jurek.blaszczynski@cs.put.poznan.pl">jurek.blaszczynski@cs.put.poznan.pl</a>)
 * @author Marcin Szeląg (<a href="mailto:marcin.szelag@cs.put.poznan.pl">marcin.szelag@cs.put.poznan.pl</a>)
 */
@RestController
public class RuleRankerController {
		
	/**
	 * Default rule set ID.
	 */
	public static final int DEFAULT_RULE_SET_ID = 1;
	
	/**
	 * Application environment.
	 */
	@Autowired
	private Environment environment;
	
	/**
	 * Logger
	 */
	private static final Logger logger = LoggerFactory.getLogger(RuleRankerController.class);
	
	/**
	 * Repository with users.
	 */
	@Autowired
	private UsersRepository usersRepository;
		
	/**
	 * Repository with models.
	 */
	@Autowired
	private ModelsRepository modelsRepository;
		
	/**
	 * Logs environment properties.
	 */
	@PostConstruct
	void logEnvironmentProperties() {
		logger.info("DB Configuration: spring.datasource.url = " + environment.getProperty("spring.datasource.url"));
		logger.info("DB Configuration: spring.datasource.driver-class-name = " + environment.getProperty("spring.datasource.driver-class-name"));
		logger.info("DB Configuration: spring.datasource.username = " + environment.getProperty("spring.datasource.username"));    		
	}
	
	/**
	 * Provides classifier {@link SimpleRuleClassifier} on the basis of the provided model {@link Models}.
	 * 
	 * @param model model with rules, which are used for classification
	 * @param attributes an array of attributes {@link Attribute} describing model
	 * @return classifier {@link SimpleRuleClassifier}
	 */
	private SimpleRuleClassifier getClassifier(Models model, Attribute[] attributes) {
		RuleSet rules = null;
		SimpleClassificationResult defaultResponse = null;
		SimpleRuleClassifier classifier = null;
		
		if (model != null) {
			Attribute [] theaseAttributes = null;
			if (attributes == null) {
				theaseAttributes = getAttributes(model);
			}
			else {
				theaseAttributes = attributes;
			}
			if (theaseAttributes != null) {
				rules = getRuleSet(model);
				if (rules != null) {
					//set mean (median) value (according to the values of the first active enumeration decision attribute)
					for (int i = 0; i < theaseAttributes.length; i++) {
						if((theaseAttributes[i].isActive()) && (theaseAttributes[i] instanceof EvaluationAttribute)) {
							EvaluationAttribute attribute = (EvaluationAttribute)theaseAttributes[i];
							if ((attribute.getType() == AttributeType.DECISION) && (attribute.getValueType() instanceof EnumerationField)) {
								ElementList classLabels = ((EnumerationField)attribute.getValueType()).getElementList();
								defaultResponse = new SimpleClassificationResult(new SimpleDecision(EnumerationFieldFactory.getInstance().create(classLabels, 
										classLabels.getSize()/2, attribute.getPreferenceType()), i));
								logger.info("Set default response (rank): " + defaultResponse.getSuggestedDecision().getEvaluation(i));
								break;
							}
						}
					}
					classifier = new SimpleRuleClassifier(rules, defaultResponse);
				}
			}			
		}
		
		return classifier;
	}
	
	/**
	 * Provides set of rules {@link RuleSet} on the basis of the provided model {@link Models}.
	 * 
	 * @param model model with rules to be provided
	 * @return set of rules {@link RuleSet}
	 */
	private RuleSet getRuleSet(Models model) {
		RuleSet rules = null;
		
		if (model != null) {
			logger.info("Retrieved rules with id: " + model.getRules().getId());
			Attribute [] attributes = getAttributes(model);  
			if (attributes != null) {
				Map<Integer, RuleSet> allRules = null;
				RuleParser ruleParser = new RuleParser(attributes);
				try (InputStream rulesStream = IOUtils.toInputStream(model.getRules().getData(), StandardCharsets.UTF_8.name())) {
					allRules = ruleParser.parseRules(rulesStream);
				}
				catch (IOException ex) {
					logger.error(ex.toString());
				}
				if ((allRules != null) && (allRules.size() > 0)) {
					rules = allRules.get(DEFAULT_RULE_SET_ID);
				}
			}			
		}
		
		return rules;
	}
	
	/**
	 * Provides an array of attributes {@link Attribute} on the basis of the provided model {@link Models}.
	 * 
	 * @param model model with attributes to be provided
	 * @return an array of attributes {@link Attribute}
	 */
	private Attribute[] getAttributes(Models model) {
		Attribute[] attributes = null;
		
		if (model != null) {
			logger.info("Retrieved meta-data with id: " + model.getRules().getMetadata().getId());
			AttributeParser attributeParser = new AttributeParser();
			try (StringReader attributeReader = new StringReader(model.getRules().getMetadata().getData())) {
				attributes = attributeParser.parseAttributes(attributeReader);
			}
			catch (IOException ex) {
				logger.error(ex.toString());
			}
		}
		
		return attributes;
	}
	
	/**
	 * Provides a model {@link Models} for given ID of a user and/or ID of a model. 
	 * 
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken)
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken)
	 * @return model {@link Models} or {@code null} when it is impossible to obtain one 
	 */
	private Models getModel (String userId, Long modelId) {
		Users user = null;
		Models model = null;
		
		if (userId != null) {
			if (usersRepository.findById(userId).isPresent()) {
				user = usersRepository.findById(userId).get();
				if (modelId != null) {
					if (modelsRepository.findById(modelId).isPresent()) {
						model = modelsRepository.findById(modelId).get();
					}
				}
				else if (user.getActiveModel() != null) {
					model = user.getActiveModel();
				}
			}
		}
		else {
			if (modelId != null) {
				if (modelsRepository.findById(modelId).isPresent()) {
					model = modelsRepository.findById(modelId).get();
				}
			}
		}
		
		return model;
	}
	
	/**
	 * HTTP endpoint providing information about the meta-data (attributes) used in ranking.
	 *  
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken)
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken)
	 * @return meta-data (attributes)
	 */
	@RequestMapping(value = "/metadata", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString getMetadataDuplicate(@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
		
		logger.info("Endpoint /metadata started...");
    	
		Models model = getModel(userId, modelId);
		JSONString result = new JSONString("{}");
		
		if (model != null) {
			logger.info("Retrieved metadata id: " + model.getRules().getMetadata().getId());
			result.setContent(model.getRules().getMetadata().getData());
		}
		else {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Model not found.");
		}
	
		return result;
	}
	
	/**
	 * HTTP endpoint providing information about the meta-data (attributes) used in ranking.
	 *  
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken)
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken)
	 * @return meta-data (attributes)
	 */
	@RequestMapping(value = "/meta-data", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public @ResponseBody JSONString getMetadata(@RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
		
		logger.info("Endpoint /meta-data started...");
    	
		Models model = getModel(userId, modelId);
		JSONString result = new JSONString("{}");
		
		if (model != null) {
			logger.info("Retrieved meta-data id: " + model.getRules().getMetadata().getId());
			result.setContent(model.getRules().getMetadata().getData());
		}
		else {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Model not found.");
		}
	
		return result;
	}
		
	/**
	 * HTTP endpoint providing information about the rule set model used in ranking.
	 * 
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken)
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken)
	 * @return text representation of the rule set model
	 */
	@RequestMapping(value = "/rules-text", method = RequestMethod.GET, produces = "text/plain;charset=UTF-8")
    public @ResponseBody String getRulesText(@RequestParam(value="user_id", required=false) String userId, 
		@RequestParam(value="model_id", required=false) Long modelId) {
		StringBuilder stringBuilder = new StringBuilder();
		
		logger.info("Endpoint /rules-text started...");
    	
		Models model = getModel(userId, modelId);
		String result = "";
		
		if (model != null) {
			RuleSet rules = getRuleSet(model); 
			for (int i = 0; i < rules.size(); i++) {
				stringBuilder.append(i+1).append(": ").append(rules.getRule(i).toString()).append("\n");
			}
			result = stringBuilder.toString(); 
		}
		else {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Model not found.");
		}
		
		
		return result;
	}
		
	/**
	 * HTTP endpoint providing information about the rule set model used in ranking.
	 * 
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken)
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken)
	 * @return ruleML representation of the rule set model
	 */
	@RequestMapping(value = "/rules-ruleml", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    public @ResponseBody String getRulesRuleML(@RequestParam(value="user_id", required=false) String userId, 
		@RequestParam(value="model_id", required=false) Long modelId) {
		
		logger.info("Endpoint /rules-ruleml started...");
    	
		Models model = getModel(userId, modelId);;
		String result = "";
		
		if (model != null) {
			logger.info("Retrieved rules id: " + model.getRules().getId());
			result = model.getRules().getData(); 
		}
		else {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Model not found.");
		}
		
		return result;
	}
    
    /**
	 * HTTP endpoint producing ranking for meta-alerts provided as input.
	 *  
	 * @param input a JSON array of meta-alerts
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken)
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken)
	 * @return ranking of meta-alerts
	 */
    @RequestMapping(value = "/rank", method = RequestMethod.POST, consumes = {"application/json;charset=UTF-8", "text/plain;charset=UTF-8"}, produces = "application/json;charset=UTF-8")
    public @ResponseBody Result [] rank(@RequestBody String input, @RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
    	
    	logger.info("Endpoint /rank started...");
    	
		Result [] results = null;
		Models model = getModel(userId, modelId);
		
		if (model != null) {
			Attribute[] attributes = getAttributes(model);
			if (attributes != null) {
				ObjectParser objectParser = new ObjectParser.Builder(attributes).build();
				InformationTable informationTable = null;
				try (StringReader objectReader = new StringReader(input)) {
					informationTable = objectParser.parseObjects(objectReader);
				}
				catch (IOException ex) {
					throw new ResponseStatusException(
					          HttpStatus.BAD_REQUEST, "Error while parsing prioritisation examples: " + ex.toString());
				}
				
				if (informationTable != null) {
					SimpleRuleClassifier classifier = getClassifier(model, attributes);
					SimpleDecision decision = null;
					IntIterator iterator = null;
					
					results = new Result [informationTable.getNumberOfObjects()];
					int identifierIndex = informationTable.getActiveIdentificationAttributeIndex();
					for (int i = 0; i < informationTable.getNumberOfObjects(); i++) {
						decision = classifier.classify(i, informationTable).getSuggestedDecision();
						iterator = decision.getAttributeIndices().iterator();
						
						while(iterator.hasNext()) {
							int j = iterator.nextInt();
							results[i] = new Result(informationTable.getField(i, identifierIndex).toString(), decision.getEvaluation(j).toString());
						}
					}
				}
				else {
					throw new ResponseStatusException(
					          HttpStatus.BAD_REQUEST, "Error while parsing prioritisation examples.");
				}
			}
			else {
				throw new ResponseStatusException(
				          HttpStatus.BAD_REQUEST, "Meta-data not valid.");
			}
		}
		else {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Model not found.");
		}
		
		return results;
    }
    
    /**
	 * HTTP endpoint producing ranking for meta-alerts provided as input. This extended version produces descriptions of meta alerts with associated ranks.
	 *   
	 * @param input a JSON array of meta-alerts
	 * @param userId ID of a user in database (when this ID is {@code null} model ID is taken)
	 * @param modelId ID of a model in database (when this ID is {@code null} and user ID is not {@code null}, active model for the user with {@code userID} is taken)
	 * @return descriptions of meta-alert with associated ranks 
	 */
    @RequestMapping(value = "/rank-ext", method = RequestMethod.POST, consumes = {"application/json;charset=UTF-8", "text/plain;charset=UTF-8"}, produces = "application/json;charset=UTF-8")
    public @ResponseBody InformationTable rankExt(@RequestBody String input, @RequestParam(value="user_id", required=false) String userId, 
    		@RequestParam(value="model_id", required=false) Long modelId) {
    	
    	logger.info("Endpoint /rank-ext started...");
    	
    	InformationTable resultInformationTable = null;
		Models model = getModel(userId, modelId);
		
		if (model != null) {
			Attribute[] attributes = getAttributes(model);
			if (attributes != null) {
				ObjectParser objectParser = new ObjectParser.Builder(attributes).build();
				InformationTable testInformationTable = null;
				try (StringReader objectReader = new StringReader(input)) {
					testInformationTable = objectParser.parseObjects(objectReader);
				}
				catch (IOException ex) {
					throw new ResponseStatusException(
					          HttpStatus.BAD_REQUEST, "Error while parsing prioritisation examples: " + ex.toString());
				}
				
				if (testInformationTable != null) {
					SimpleRuleClassifier classifier = getClassifier(model, attributes);
					ClassificationResultSet classifiationResultSet = new SimpleClassificationResultSet(testInformationTable, classifier);
					resultInformationTable = classifiationResultSet.getInformationTableWithClassificationResults();
				}
				else {
					throw new ResponseStatusException(
					          HttpStatus.BAD_REQUEST, "Error while parsing prioritisation examples.");
				}
			}
			else {
				throw new ResponseStatusException(
				          HttpStatus.BAD_REQUEST, "Meta-data not valid.");
			}
		}
		else {
			throw new ResponseStatusException(
			          HttpStatus.NOT_FOUND, "Model not found.");
		}
		
		return resultInformationTable;
    }
}