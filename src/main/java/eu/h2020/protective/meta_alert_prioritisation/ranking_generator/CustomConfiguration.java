/**
 * Copyright (C) Jerzy Błaszczyński, Marcin Szeląg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.h2020.protective.meta_alert_prioritisation.ranking_generator;

import java.util.ArrayList;
import java.util.Collection;

import org.rulelearn.data.InformationTable;
import org.rulelearn.data.json.InformationTableSerializer;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.h2020.protective.meta_alert_prioritisation.ranking_generator.model.json.JSONString;
import eu.h2020.protective.meta_alert_prioritisation.ranking_generator.model.json.JSONStringSerializer;
import eu.h2020.protective.meta_alert_prioritisation.ranking_generator.model.json.JSONToStringDeserializer;

/**
 * Custom configuration of Spring Boot registering Gson as converter of HTTP requests and responses.
 *
 * @author Jerzy Błaszczyński (<a href="mailto:jurek.blaszczynski@cs.put.poznan.pl">jurek.blaszczynski@cs.put.poznan.pl</a>)
 * @author Marcin Szeląg (<a href="mailto:marcin.szelag@cs.put.poznan.pl">marcin.szelag@cs.put.poznan.pl</a>)
 */
@Configuration
public class CustomConfiguration {
	
    @Bean
    public HttpMessageConverters customConverters() {
        Collection<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        GsonHttpMessageConverter gsonHttpMessageConverter = new GsonHttpMessageConverter();
        
        GsonBuilder gsonBuilder = new GsonBuilder();
        /*
        // attributes
		gsonBuilder.registerTypeAdapter(Attribute.class, new AttributeDeserializer());
		gsonBuilder.registerTypeAdapter(IdentificationAttribute.class, new IdentificationAttributeSerializer());
		gsonBuilder.registerTypeAdapter(EvaluationAttribute.class, new EvaluationAttributeSerializer());
		*/
		// information table
		gsonBuilder.registerTypeAdapter(InformationTable.class, new InformationTableSerializer());
		
		// String representation of JSON 
		gsonBuilder.registerTypeAdapter(JSONString.class, new JSONStringSerializer());
		gsonBuilder.registerTypeAdapter(String.class, new JSONToStringDeserializer());
		
		Gson gson = gsonBuilder.create();
        
		gsonHttpMessageConverter.setGson(gson);
        messageConverters.add(gsonHttpMessageConverter);

        return new HttpMessageConverters(true, messageConverters);
    }
}