@ECHO OFF

REM pass Java 1.8 JDK path (absolute) as the first argument

IF "%~1" == "" GOTO :usage

REM Go to main project directory
CD ..

REM Clean first
call docker stop rg
call docker rm rg
call docker stop postgresdb
call docker rm postgresdb

call docker network rm protective-net

SET JAVA_HOME=%1%
SET PATH=%JAVA_HOME%\bin;%PATH%

call gradlew clean build createDockerfile -x test
call docker build -t ranking-generator build/docker

REM save some disk space...
del build\docker\ranking-generator*.war
del build\libs\ranking-generator*.war

REM Create common network for both containers
call docker network create protective-net

REM Start postgres DB
call docker run --name postgresdb --rm -p 5432:5432 -e POSTGRES_DB=protective -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=mysecretpassword -d --net=protective-net postgres

REM Start ranking-generator with env
call docker run --name rg --rm -it -p 8008:8008 -e WAIT_FOR_HOST_AND_PORT=postgresdb:5432 -e SPRING_DATASOURCE_URL=jdbc:postgresql://postgresdb:5432/protective -e SPRING_DATASOURCE_USERNAME=postgres -e SPRING_DATASOURCE_PASSWORD=mysecretpassword --net=protective-net ranking-generator:latest

pause

:usage
echo Wrong number of parameters. Exactly one parameter required.
echo ------
echo Usage:
echo ------
echo protective-simple-launcher "<java-1.8-jdk-path>"