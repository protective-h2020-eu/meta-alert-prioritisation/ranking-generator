@ECHO OFF

REM pass Java 1.8 JDK path (absolute) as the first argument

IF "%~1" == "" GOTO :usage

REM Go to main project directory
CD ..

REM Clean first
call docker stop rg
call docker rm rg
call docker stop postgresdb
call docker rm postgresdb

SET JAVA_HOME=%1%
SET PATH=%JAVA_HOME%\bin;%PATH%

call gradlew clean build createDockerfile -x test

REM save some disk space...
del build\libs\ranking-generator*.war

start docker-compose up --build

echo(
echo Docker-compose is starting in separate console... Once all services are up, you can safely press any key to end this script.
pause >nul

REM save some disk space...
del build\docker\ranking-generator*.war

:usage
echo Wrong number of parameters. Exactly one parameter required.
echo ------
echo Usage:
echo ------
echo protective-compose-launcher "<java-1.8-jdk-path>"