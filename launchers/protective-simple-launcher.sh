#!/usr/bin/env sh

# Pass Java 1.8 JDK path (absolute) as the first argument

# Go to main project directory
cd ..

# Clean first
docker stop rg
docker rm rg
docker stop postgresdb
docker rm postgresdb

docker network rm protective-net

export JAVA_HOME=$1
export PATH=$JAVA_HOME\bin:$PATH

./gradlew clean build createDockerfile -x test
docker build -t ranking-generator build/docker

# Save some disk space...
#del build\docker\ranking-generator*.war
#del build\libs\ranking-generator*.war

# Create common network for both containers
docker network create protective-net

# Start postgres DB
docker run --name postgresdb --rm -p 5432:5432 -e POSTGRES_DB=protective -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=mysecretpassword -d --net=protective-net postgres

# Start ranking-generator with env
docker run --name rg --rm -it -p 8008:8008 -e WAIT_FOR_HOST_AND_PORT=postgresdb:5432 -e SPRING_DATASOURCE_URL=jdbc:postgresql://postgresdb:5432/protective -e SPRING_DATASOURCE_USERNAME=postgres -e SPRING_DATASOURCE_PASSWORD=mysecretpassword --net=protective-net ranking-generator:latest
