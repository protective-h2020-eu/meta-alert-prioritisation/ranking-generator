#!/bin/sh

echo "Configuring ranking-generator..."

# Wait for DB to be ready:
if [ -n "$WAIT_FOR_HOST_AND_PORT" ]; then
	HOST_AND_PORT=$WAIT_FOR_HOST_AND_PORT
	echo "Started waiting for $HOST_AND_PORT"

	/app/wait-for-it.sh $HOST_AND_PORT -- java -jar /app/ranking-generator.war
fi

# echo "Starting ranking-generator..."
# java -jar /app/ranking-generator.war